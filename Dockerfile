FROM node:18-alpine

USER node

WORKDIR /app

COPY package*.json /app/

RUN npm ci && npm cache clean --force

CMD ["npm", "run", "start"]
