import { render, screen } from '@testing-library/angular';
import { AppComponent } from './app.component';

describe('AppComponent', () => {
  it('has greeting', async () => {
    await render(AppComponent);

    const heading = screen.getByRole('heading');

    expect(heading).toHaveTextContent('Welcome');
  });
});
